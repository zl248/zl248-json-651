package edu.duke.ece651.classbuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//  class: help to generate deserializer code of every class
public class DeserializerGenerator {
  private int ID;
  private String ClassName;
  private JSONObject clssObj;
  private JSONArray FldArray;
  private String sp = "    ";
  private ArrayList<String> JsClssNms;
  private JSONArray JsClssArry;
  private HashMap<String, Integer> refMap;
  private String lastName;
  public void setJsID(int ID) {
    this.ID = ID;
  }

  // initialization method, put in the necessary data structures and info into this class
  public void setJsClass(String ClassName, JSONObject clssObj, ArrayList<String> JsClssNms, JSONArray JsClssArry) {
    this.ClassName = ClassName;
    this.clssObj = clssObj;
    this.FldArray = clssObj.getJSONArray("fields");
    this.JsClssNms = JsClssNms;
    this.JsClssArry = JsClssArry;
    this.refMap = new HashMap<String, Integer>();
    this.lastName = "";
  }

  // main funciton: generate deserializer code
  public String getDeserCode() throws IOException{
    this.refMap.put(this.ClassName, this.ID);
    String Deser = "  public static " + this.ClassName + " read" + this.ClassName;
    Deser += "(JSONObject js)throws JSONException{" + "\n";
    Deser += getObjCode(this.ClassName, 0, this.FldArray, "", 0); // for each class
    Deser += this.sp + "return My" + this.ClassName +";"+ "\n";
    Deser += "  }" + "\n";
    return Deser;
  }

  // manipute json object code
  public String getObjCode(String ClassName, int ID, JSONArray FldArray, String fieldName, int isAry) throws IOException{
    String NewObjStr = "";
    NewObjStr += this.sp + ClassName + " My" + ClassName + " = ";
    NewObjStr += "new " + ClassName + "();" + "\n";
    if (ID == 0) {
      NewObjStr += this.sp + "JSONObject js" + ClassName + " = js;" + "\n";
    }
    else {
      if (isAry == 1) {
        NewObjStr += this.sp + "JSONObject js" + ClassName + " = js" + this.lastName + ".getJSONObject(\"values\").getJSONArray(\"" + fieldName +"\").getJSONObject(0);" + "\n";
      }
      else {
        NewObjStr += this.sp + "JSONObject js" + ClassName + " = js" + this.lastName + ".getJSONObject(\"values\").getJSONObject(\"" + fieldName +"\");" + "\n";
      }
    }
    NewObjStr += this.sp + "JSONArray "+ ClassName+ "fields = js" + ClassName +".getJSONArray(\"order\");" + "\n";
    NewObjStr += this.sp + "JSONObject " + ClassName +"value = js"+ClassName+".getJSONObject(\"values\");" + "\n";
    
    // set the fields in the object
    //System.out.println(ClassName + this.FldArray.length());
    for (int i = 0; i < FldArray.length(); ++i) {
      JSONObject field = FldArray.getJSONObject(i);
      NewObjStr += ObjFieldInit(ClassName, i, ID, field, FldArray);
    }
    return NewObjStr;
  }

  // manipulate the class's field code, would invoke getobjcode when necessary
  public String ObjFieldInit(String ClassName, int i, int ID, JSONObject field, JSONArray FldArray) throws IOException{
    this.refMap.put(ClassName, ID);
    String FieldStr = "";
    if (isArray(field)) {
      if(isArrayPrim(field)){
        System.out.println(field);
        String field_name = field.getString("name");
        FieldStr += this.sp + "JSONArray " + field_name + "Array" + " = ";
        FieldStr += ClassName+"value" + ".getJSONArray(\"" + field_name + "\");" + "\n";
        // for loop
        FieldStr += this.sp + "for (int " + field_name + " = 0; ";
        FieldStr += field_name + " < " + field_name + "Array.length(); ";
        FieldStr += field_name + "++){" + "\n";
        FieldStr += this.sp + this.sp + "My"+ ClassName + ".add" + FirstCap(field_name);
        String field_type = field.getJSONObject("type").getString("e");
        String addin = "";
        if (field_type.equals("byte") || field_type.equals("short") || field_type.equals("char")) {
          addin = "Int";
        }
        FieldStr += "(" +"("+field_type+ ")"+ field_name + "Array.get"+addin + "(" + field_name + "));" + "\n";
        FieldStr += this.sp + "}" + "\n";
      }
      else{ // object array
        if (!this.refMap.containsKey(field.getJSONObject("type").getString("e"))){
          JSONObject NewObj = new JSONObject();
          int k = -1;
          for (int j =0; j < this.JsClssArry.length(); ++j){
            JSONObject currObj = this.JsClssArry.getJSONObject(j);
            System.out.println("currobj" + currObj.getString("name"));
            System.out.println("newobj" + field.getString("name"));
            if (currObj.getString("name").equals(field.getJSONObject("type").getString("e"))){
              k = j;
              NewObj = currObj;
            }
          }
          if (k == -1){
            throw new IOException("there is no such class to invoke and deserialize" + field.getString("type"));
          }
          this.lastName = ClassName;
          FieldStr += getObjCode(field.getJSONObject("type").getString("e"), ID + 1, NewObj.getJSONArray("fields"), field.getString("name"), 1);
          refMap.put(field.getJSONObject("type").getString("e"), ID + 1);
        }
          String field_name = field.getString("name");
          FieldStr += this.sp + "JSONArray " + field_name + "Array" + " = ";
          FieldStr += ClassName+"value" + ".getJSONArray(\"" + field_name + "\");" + "\n";
          FieldStr += this.sp + "for (int " + field_name + " = 0; ";
          FieldStr += field_name + " < " + field_name + "Array.length(); ";
          FieldStr += field_name + "++){" + "\n";
          FieldStr += this.sp + this.sp + "My"+ ClassName + ".add" + FirstCap(field_name);
          FieldStr += "(My" + field.getJSONObject("type").getString("e") + ");" + "\n";
          FieldStr += this.sp + "}" + "\n";
      }
    }
    else{ // sigle prim element..
      if (isPrim(field)){
        FieldStr += this.sp + "My" + ClassName + ".set" + FirstCap(field.getString("name"));
        String temPrimType = field.getString("type");
        String addin = "";
        if (temPrimType.equals("short") || temPrimType.equals("byte") || temPrimType.equals("char")){
            addin = "Int";
          }
        FieldStr += "("+"("+temPrimType+ ")" + ClassName+"value.get"+addin + "("+ClassName+"fields.getString(";
        FieldStr += i + ")));" + "\n";
      }
      else{// single object
        if (this.refMap.containsKey(field.getString("type"))) {// contains ref
          FieldStr += this.sp + "My" + ClassName + ".set" + FirstCap(field.getString("name"));
          FieldStr += "(My" + field.getString("type") + ");" + "\n";
        }
        else {// not contain ref
          JSONObject NewObj = new JSONObject();
          int k = -1;
          for (int j =0; j < this.JsClssArry.length(); ++j){
            JSONObject currObj = this.JsClssArry.getJSONObject(j);
            System.out.println("currobj" + currObj.getString("name"));
            System.out.println("newobj" + field.getString("name"));
            if (currObj.getString("name").equals(field.getString("type"))){
              k = j;
              NewObj = currObj;
            }
          }
          if (k == -1){
            throw new IOException("there is no such class to invoke and deserialize" + field.getString("type"));
          }
          this.lastName = ClassName;
          FieldStr += getObjCode(field.getString("type"), ID + 1, NewObj.getJSONArray("fields"), field.getString("name"), 0);
          FieldStr += this.sp + "My" + ClassName + ".set" + FirstCap(field.getString("name"));
          FieldStr += "(My" + field.getString("type") + ");" + "\n";
          refMap.put(field.getString("type"), ID+1);
        }   
      }
    }
    return FieldStr;
  }

  // helper funciton, capitalize the first char
  public String FirstCap(String NoCap) {
    String first = NoCap.substring(0, 1);
    String after = NoCap.substring(1);
    first = first.toUpperCase();
    return first + after;
  }

  // helper funcitons: as the same as the one in serializer
  public Boolean isArray(JSONObject jsonfield) {
    JSONObject Earray = new JSONObject();
    try{
      Earray = jsonfield.getJSONObject("type");
      Earray.get("e");
    }
    catch(JSONException e){
      return false; // then it is a single element
    }
    
    try{
      Earray = jsonfield.getJSONObject("type");
      Earray.getString("e");
    }
    catch (JSONException e) {
      System.out.println("this program cannot handle multi-dimention array");
      e.printStackTrace();
    }
    return true;
  }

  public Boolean isPrim(JSONObject jsonfield) {
    int k = -1;
    for (int i = 0; i < this.JsClssNms.size(); ++i) {
      if (jsonfield.getString("type").equals(this.JsClssNms.get(i))) {
        k++;
      }
    }
    if (k == -1) {
      return true;
    }
    return false;
  }
  
  public Boolean isArrayPrim(JSONObject jsonfield) {
    JSONObject currType = jsonfield.getJSONObject("type");
    int k = -1;
    for (int i = 0; i < this.JsClssNms.size(); ++i) {
      if (currType.getString("e").equals(this.JsClssNms.get(i))) {
        k++;
      }
    }
    if (k == -1) {
      return true;
    }
    return false;
  } 
  
}
