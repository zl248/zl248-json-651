package edu.duke.ece651.classbuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class SourceCodeGenerator {
  public String ClassName;
  // field's array
  private JSONArray FieldArry; 
  // package name
  private String Pkg;
  // object field: field parser
  private FieldParser parser;
  // global field: used to represent the text as source code
  private String SourceCode;
  // store the conversion of wrapper type and primitive type
  private HashMap<String, String> Wrapper;
  // global identity
  private int ID;
  // object field: serializer, to output toJSON funciton
  private Serializer Seri;
  // global class array, for serializer to search and match
  private JSONArray JsClssAry;
  // global class names
  private ArrayList<String> JsClssNms;
  
  // set methods: initialize
  public void setJsID(int ID) {
    this.ID = ID;
  }
  
  // method 1: add package
  public void setJsPkg(String Pkg) {
    this.Pkg = Pkg;
  }
  
  // method 2: add a JSON classname // as a constructor role
  public void setJsClass(String className, JSONObject SpecJsObj) {
    this.ClassName = className;
    this.FieldArry = SpecJsObj.getJSONArray("fields");
    this.parser = new FieldParser(this.FieldArry);
    this.Wrapper = new HashMap<String, String>();
    this.Wrapper.put("byte", "Byte");
    this.Wrapper.put("short", "Short");
    this.Wrapper.put("int", "Integer");
    this.Wrapper.put("long", "Long");
    this.Wrapper.put("float", "Float");
    this.Wrapper.put("double", "Double");
    this.Wrapper.put("char", "Character");
    this.Wrapper.put("boolean", "Boolean");
  }

  // method 3: put in the whole array of class into generator
  public void setJsArry(JSONArray JsClssAry) {
    this.JsClssAry = JsClssAry;
  }

  // method 4: put in the whole class'list into generator
  public void setJsClssNms(ArrayList<String> JsClssNms) {
    this.JsClssNms = JsClssNms;
  }
  // main method : get source code
  // return to the classbuilder (main class)
  public String getSrcCode() throws IOException{
    SourceCode = "";
    PrintPkg();
    PrintImport();
    PrintClass();
    return this.SourceCode;
  }

  // individual print methods: print pkg
  public void PrintPkg(){
    if (this.Pkg != null){
      this.SourceCode += "package " + this.Pkg +";" +  "\n" + "\n";
    }
  }

  // invoke diffrent part of codes' funciton and print to the global field: SourceCode
  // 4 parts
  public void PrintClass() throws IOException {
    // class
    PrintClassName();
    PrintLeftParenthesis();
    this.SourceCode += "\n";
    // fields
    PrintFields();
    this.SourceCode += "\n";
    // constructor
    PrintConstructor();
    this.SourceCode += "\n";
    // methods
    PrintMethods();
    PrintSerializatoin();
    PrintRightParenthesis();
  }

  // text implementation
  public void PrintClassName() {
    this.SourceCode += "public class " + this.ClassName + " ";
  }

  public void PrintLeftParenthesis() {
    this.SourceCode += "{" + "\n";
  }

  public void PrintRightParenthesis() {
    this.SourceCode += "\n" + "}";
  }

  public void PrintRightParenthesis_mtd() {
    this.SourceCode += "\n" +"  " + "}";
  }
  
  // invoke another helper class: fieldparser
  // use a loop to print all the fields
  public void PrintFields() {
    for (int i = 0; i < this.parser.num; i++) {
      this.SourceCode += "  " + "private " + this.parser.getFieldType(i);
      this.SourceCode += " " + this.parser.getFieldName(i) + ";" + "\n";
    }
  }

  public void PrintConstructor() {
    if (!this.parser.hasArray()) {
      return;
    }
    this.SourceCode += "  " + "public " + this.ClassName + "()";
    PrintLeftParenthesis();
  
    for (int i = 0; i < this.parser.num; i++){
      if (this.parser.isArray(i)){
        this.SourceCode += "    " + "this." + this.parser.getFieldName(i) + " = ";
        this.SourceCode += "new " + this.parser.getFieldType(i) + "();" + "\n";
      } 
    }
    
    this.SourceCode += "  }";
  }

  // divide nonArray method and Array method into differnt functions to implement
  public void PrintMethods() {
    for (int i = 0; i < this.parser.num; i++) {
      if (!this.parser.isArray(i)){
        PrintNonArrayMtd(this.parser.getFieldName(i), this.parser.getFieldType(i));
      }
      else {
        PrintArrayMtd(this.parser.getFieldName(i), this.parser.getFieldType(i));
      }
    }
  }

  // main function responsible for non array method
  // two arguments: field name and fieldtype
  // implement two methods: get and set
  public void PrintNonArrayMtd(String FieldName, String FieldType) {
    PrintGetMtd(FieldName, FieldType);
    PrintSetMtd(FieldName, FieldType);
  }

  // main fucntion responsible for array method
  // two arguments: field name and fieldtype
  // implement four methods: num, add, get, set
  // throw exception when it is in multi-dimensional object
  public void PrintArrayMtd(String FieldName, String FieldType) {
    String Method_fieldtype = "";
    try{
      for (int i = 0; i < this.FieldArry.length(); ++i) {
        if (this.FieldArry.getJSONObject(i).getString("name").equals(FieldName)) {
          Method_fieldtype = this.FieldArry.getJSONObject(i).getJSONObject("type").getString("e");
        } 
      }
    } catch (JSONException e) {
      System.out.println(e);
      System.out.println("my program does not yet support multi-array-method display");
    }
    PrintAryNumMtd(FieldName, Method_fieldtype); 
    PrintAryAddMtd(FieldName, Method_fieldtype);
    PrintAryGetMtd(FieldName, Method_fieldtype);
    PrintArySetMtd(FieldName, Method_fieldtype);
  }

  // detailed implementation: single element get method
  public void PrintGetMtd(String FieldName, String FieldType) {
    this.SourceCode += "  " + "public " + FieldType + " get" + FirstCap(FieldName) + "()";
    PrintLeftParenthesis();
    this.SourceCode += "    " + "return " + "this." + FieldName + ";";
    PrintRightParenthesis_mtd();
    this.SourceCode += "\n";
  }

  // single element set method
  // set a single element into the object
  public void PrintSetMtd(String FieldName, String FieldType) {
    this.SourceCode += "  " + "public " + "void " + "set" + FirstCap(FieldName) + "(";
    this.SourceCode += FieldType + " " + FieldName + ")";
    PrintLeftParenthesis();
    this.SourceCode += "    " + "this." + FieldName + " = " + FieldName + ";";
    PrintRightParenthesis_mtd();
    this.SourceCode += "\n";
  }

  // array element num method
  // "return" the number of elements in the current array
  public void PrintAryNumMtd(String FieldName, String FieldType){
    this.SourceCode += "  " + "public " + "int " + "num" + FirstCap(FieldName) + "()";
    PrintLeftParenthesis();
    this.SourceCode += "    " + "return " + "this." + FieldName + "." + "size();";
    PrintRightParenthesis_mtd();
    this.SourceCode += "\n";
  }

  // array element add method
  // using arraylist operation: add
  public void PrintAryAddMtd(String FieldName, String FieldType) {
    this.SourceCode += "  " + "public " + "void " + "add" + FirstCap(FieldName) + "(";
    this.SourceCode += FieldType + " n"+ ")";
    PrintLeftParenthesis();
    this.SourceCode += "    " + "this." + FieldName + ".add" + "(n);";
    PrintRightParenthesis_mtd();
    this.SourceCode += "\n";
  }

  // array element get method
  // generated code need one argument: index
  public void PrintAryGetMtd(String FieldName, String FieldType) {
    this.SourceCode += "  " + "public " + FieldType + " get" + FirstCap(FieldName) + "(";
    this.SourceCode += "int" + " index"+ ")";
    PrintLeftParenthesis();
    this.SourceCode += "    " + "return " + "this." + FieldName + ".get(index);";
    PrintRightParenthesis_mtd();
    this.SourceCode += "\n";
  }
  
  // array element set method
  // generated code need two argument: index and n
  // the position and object information
  public void PrintArySetMtd(String FieldName, String FieldType) {
    this.SourceCode += "  public " + "void " + "set" + FirstCap(FieldName) + "(";
    this.SourceCode += "int " + "index," + FieldType  +" n)";
    PrintLeftParenthesis();
    this.SourceCode += "    " + "this." + FieldName + ".set(index, n);";
    PrintRightParenthesis_mtd();
    this.SourceCode += "\n";
  }

  // helper funciton: use to capitalize the first char
  // one argument: no capitalized string
  // return capitalized string
  public String FirstCap(String NoCap) {
    String first = NoCap.substring(0, 1);
    String after = NoCap.substring(1); 
    first = first.toUpperCase();
    return first + after;
  }

  // serialization helper function
  // invoke serializer class to print toJSON function
  public void PrintSerializatoin() throws IOException{
    this.SourceCode += "  JSONObject toJSON() throws JSONException";
    PrintLeftParenthesis();
    this.Seri = new Serializer(this.ID, this.ClassName, this.FieldArry, this.JsClssAry, this.JsClssNms);
    ArrayList<String> classHis = new ArrayList<String>();
    this.SourceCode += this.Seri.printAll(this.ID, this.ClassName, this.FieldArry, classHis);
    PrintRightParenthesis();
  }

  // print import 
  public void PrintImport() {
    this.SourceCode += "\n";
    this.SourceCode += "import java.util.*;" + "\n";
    this.SourceCode += "import org.json.*;" + "\n";
  }
}
