package edu.duke.ece651.classbuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// serializer, help SourceCode generator to output the toJSON() function
public class Serializer {
  // three prim field
  private int ID;
  private String ClassName;
  private JSONArray FldArry;
  private String start;
  private int length;
  private HashMap<String, Integer> refMap;
  private HashSet<String> primSet;
  private JSONArray JsClssArry;
  private ArrayList<String> JsClssNms;
  private HashSet<String> refhis;

  // constructor
  public Serializer(int ID, String ClassName, JSONArray FldArry, JSONArray JsClssArry, ArrayList<String> JsClssNms) {
    this.ID = ID;
    this.ClassName = ClassName;
    this.FldArry = FldArry;
    this.start = "    ";
    this.length = this.FldArry.length();
    this.JsClssArry = JsClssArry;
    this.refMap = new HashMap<String, Integer>();
    this.JsClssNms = JsClssNms;
    this.primSet = new HashSet<String>();
    this.primSet.add("byte");
    this.primSet.add("short");
    this.primSet.add("int");
    this.primSet.add("long");
    this.primSet.add("float");
    this.primSet.add("double");
    this.primSet.add("char");
    this.primSet.add("boolean");
    this.refhis = new HashSet<String>();
  }

  // main function to implement the code text, invocable by sub printvalues' funciton
  // argument means
  // ID: current identity
  // class: use to trace the current class name
  // FldArry: use to trace the field array with the class
  // classhis: use to trace the class has been invoked
  public String printAll(int ID, String ClassName, JSONArray FldArry, ArrayList<String> classHis) throws IOException{
    String all = this.start + "JSONObject jsonObject"+ ID + " = new JSONObject();" + "\n";
    all += printID(ID) + "\n";
    all += printType(ID, ClassName) + "\n";
    this.refMap.put(ClassName, ID);
    all += printValue(ID, ClassName, FldArry, classHis) + "\n";
    all += printOrder(ID, FldArry) + "\n";
    if (ID == 0){
      all += this.start + "return jsonObject" + ID + ";";
    }
    return all;
  }

  // put id into the JSONObject
  public String printID(int ID) {
    return this.start + "jsonObject" + ID + ".put(\"id\", " + ID + ");" + "\n";
  }

  // put type into the JSONObject
  public String printType(int ID, String ClassName) {
    return this.start + "jsonObject" + ID + ".put(\"type\", \"" + ClassName + "\");" + "\n";
  }

  // put field order into the JSONObject
  public String printOrder(int ID, JSONArray FldArry) {
    String order = this.start + "JSONArray fieldOrder" + ID + " = new JSONArray();" + "\n";
    for (int i = 0; i < FldArry.length(); i++) {
      JSONObject jsonfield = FldArry.getJSONObject(i);
      order += this.start + "fieldOrder" + ID + ".put(\"" + jsonfield.getString("name") + "\");" + "\n";
    }
    order += this.start + "jsonObject" + ID + ".put(\"order\", fieldOrder" + ID + ");" + "\n";
    return order;
  }

  // put the value into the JSONObject
  public String printValue(int ID, String ClassName, JSONArray FldArry, ArrayList<String> classHis) throws IOException {
    String value = this.start + "JSONObject value" + ID;
    value += " = new JSONObject();" + "\n";
    int localID = ID;
    int fieldNum = FldArry.length();
    for (int i = 0; i < fieldNum; ++i) {
      //if (it is an object, then we need to reserialize it..)  
      JSONObject jsonfield = FldArry.getJSONObject(i); 
      if (isArray(jsonfield)) {
        classHis.add("1" + jsonfield.getString("name"));
        if (isArrayPrim(jsonfield)) {
          String tempName = jsonfield.getString("name");
          value += this.start + "JSONArray " + tempName + "Array = new JSONArray();" + "\n";
          value += this.start + "for (int " + tempName + " = 0; " + tempName + " < ";
          if (classHis.size() > 1){
            for (int g = 0; g < classHis.size() - 1; ++g){
              String h = "";
              if (classHis.get(g).substring(0, 1).equals("1")) {
                h = "0";
              }
              value += "get" + FirstCap(classHis.get(g).substring(1)) + "(" + h + ").";
            }
          }
          value += "num" + FirstCap(classHis.get(classHis.size() - 1).substring(1)) + "()";
          value += "; " + tempName + "++){" + "\n";
          value += this.start + this.start + tempName + "Array" + ".put(";
          if (classHis.size() > 1){
            for (int h = 0; h < classHis.size(); ++h){
              String addon = "";
              if (classHis.get(h).substring(0, 1).equals("1")) {
                addon = "0";
              }
              value += "get" + FirstCap(classHis.get(h).substring(1)) + "(" +addon+ ").";
            }
          }
          value += "get" + FirstCap(tempName) + "(" + tempName + "));" + "\n";
          value += this.start + "}" + "\n";
          value += this.start + "value" + localID + ".put(\"" + tempName + "\"" + ", ";
          value += tempName + "Array);" + "\n";
        }
        else { // arrayobject
          String currName = jsonfield.getString("name");
          String currType = jsonfield.getJSONObject("type").getString("e");
          if (this.refMap.containsKey(currType)) {  // situation 1: ref
            System.out.println("here!!!!!!!!!!!!contains ref!");
            int refID = refMap.get(currType);
            JSONObject ref = new JSONObject();
            ref.put("ref", refMap.get(currType));
            value += this.start + "JSONArray " + currName + "Array = new JSONArray();" + "\n";
            if (!refhis.contains(currType + "ref" + refID)) {
              refhis.add(currType + "ref" + refID);
              value += this.start + "JSONObject " + currType +"ref" + refID + " = new JSONObject();\n";
              value += this.start + currType + "ref" + refID + ".put(\"ref\", " + refID + ");" + "\n";
            }
            value += this.start + "for (int " + currName + " = 0; " + currName + " < ";
            if (classHis.size() > 1){
              for (int g = 0; g < classHis.size() - 1; ++g){
                String addon = "";
                if (classHis.get(g).substring(0, 1).equals("1")) {
                  addon = "0";
                }
                value += "get" + FirstCap(classHis.get(g).substring(1)) + "(" + addon +").";
              }
            }
            value += "num" + FirstCap(classHis.get(classHis.size() -1).substring(1)) + "();";
            value += " " + currName + "++){" + "\n";

            value += this.start + this.start + currName + "Array" + ".put(" + currType+"ref" + refID + ");" + "\n";
            value += this.start + "}" + "\n";
            value += this.start + "value" + localID + ".put(\"";
            value += currName;
            value += "\", " + currName +"Array);"+ "\n";
          }
          else {
            System.out.println("no ref, rebuild!");
            this.ID++;
            ID++;
            // initialize
            String newClassName = jsonfield.getJSONObject("type").getString("e");
            JSONArray newFldArry = new JSONArray();
            int k = -1;
            for (int j =0; j < this.JsClssArry.length(); ++j){
              JSONObject currObj = this.JsClssArry.getJSONObject(j);
              if (currObj.getString("name").equals( newClassName)){
                newFldArry =  currObj.getJSONArray("fields");
                k++;
              }
            }
            if (k == -1) {
              throw new IOException("there is no such class as the object ref" + newClassName);
            }
            value += printAll(ID, newClassName, newFldArry, classHis);
            value += this.start + "JSONArray " + "jsonarray" + (localID + 1) + " = ";
            value += "new JSONArray();" + "\n";
            // put the first element
            value += this.start + "jsonarray" + (localID + 1) + ".put(jsonObject" + (localID + 1);
            value += ");" + "\n";
            // put the rest elemtents
            JSONObject ref = new JSONObject();
            ref.put("ref", refMap.get(currType));
            int refID = refMap.get(currType);
            if (!refhis.contains(newClassName + "ref" + refID)) {
              refhis.add(newClassName + "ref" + refID);
              value += this.start + "JSONObject " + newClassName + "ref" + refID + " = new JSONObject();\n";
              value += this.start + newClassName + "ref" + refID + ".put(\"ref\", " + refID + ");" + "\n";
            }
            value += this.start + "for (int " + currName + " = 0; " + currName + " < ";
            if (classHis.size() > 1){
              for (int g = 0; g < classHis.size() - 1; ++g){
                String addon = "";
                if (classHis.get(g).substring(0, 1).equals("1")) {
                  addon = "0";
                }
                value += "get" + FirstCap(classHis.get(g).substring(1)) + "("+ addon +").";
              }
            }
            value += "num" + FirstCap(classHis.get(classHis.size() -1).substring(1)) + "()" +" - 1;";
            value += " " + currName + "++){" + "\n";
            value += this.start + this.start + "jsonarray" + (localID + 1) + ".put(" + newClassName+"ref";
            value += refID  + ");" + "\n";
            value += this.start + "}" + "\n";
            // to add the same element(ref) into the array
            value += this.start + "value" + localID + ".put(\"" + jsonfield.getString("name");
            value += "\", " + "jsonarray" + (localID + 1) + ");" + "\n";
          }
        }
      }
      else{
        classHis.add("0" + jsonfield.getString("name"));
        if (isPrim(jsonfield)){
          if (needArrayProtection(classHis)) {
            value += protectionCode_first(classHis);
          }
          value += this.start + "value" + localID + ".put(\"";
          value +=  jsonfield.getString("name");
          value += "\", ";
          if (classHis.size() > 1){
            for (int l = 0; l < classHis.size() - 1; ++l){
              String f = "";
              if (classHis.get(l).substring(0, 1).equals("1")) {
                f = "0";
              }
              value += "get" + FirstCap(classHis.get(l).substring(1)) + "(" +f+").";
            }
          }
          value += "get" + FirstCap(jsonfield.getString("name"));// a loop
          value += "());" + "\n";
          if (needArrayProtection(classHis)) {
            value += protectionCode_last(classHis, localID, jsonfield.getString("name"));
          }
        }
        else { // not prim / object
          if (this.refMap.containsKey(jsonfield.getString("type"))) {
            JSONObject ref = new JSONObject();
            int ref_type = refMap.get(jsonfield.getString("type"));
            ref.put("ref", refMap.get(jsonfield.getString("type")));
            if (!refhis.contains(jsonfield.getString("type")+ "ref" + ref_type)){
              refhis.add(jsonfield.getString("type") + "ref" + ref_type);
              value += this.start + "JSONObject " + jsonfield.getString("type") + "ref" + ref_type
                  + " = new JSONObject();\n";
              value += this.start + jsonfield.getString("type") + "ref" + ref_type + ".put(\"ref\", " + ref_type + ");\n";
            }
            value += this.start + "value" + localID + ".put(\"";
            value +=  jsonfield.getString("name");
            value += "\","+jsonfield.getString("type")+ "ref" + ref_type + ");" + "\n";
          }
          else {
            this.ID++;
            ID++;
            String newClassName = jsonfield.getString("type");
            JSONArray newFldArry = new JSONArray();
            int k = -1;
            for (int j =0; j < this.JsClssArry.length(); ++j){
              JSONObject currObj = this.JsClssArry.getJSONObject(j);
              if (currObj.getString("name").equals( newClassName)){
                newFldArry =  currObj.getJSONArray("fields");
                k++;
              }
            }
            if (k == -1) {
              throw new IOException("there is no such class as the object ref" + newClassName);
            }
            value += printAll(ID, newClassName, newFldArry, classHis);
            value += this.start + "value" + localID + ".put(\"" + jsonfield.getString("name");
            value += "\", " + "jsonObject" + (localID + 1) + ");" + "\n";
          }
        }
      }
      classHis.remove(classHis.size() - 1);
    }
    value += this.start + "jsonObject" + localID + ".put(\"values\", value" + localID + ");" + "\n";
    return value;
  }

  // helper funciton: capitalize the first char
  public String FirstCap(String NoCap) {
    String first = NoCap.substring(0, 1);
    String after = NoCap.substring(1); //substring(1)
    first = first.toUpperCase();
    return first + after;
  }

  // helper function:
  // determine if the input field's type is primitive
  public Boolean isPrim(JSONObject jsonfield) {
    int k = -1;
    for (int i = 0; i < this.JsClssNms.size(); ++i) {
      if (jsonfield.getString("type").equals(this.JsClssNms.get(i))) {
        k++;
      }
    }
    if (k == -1) {
      return true;
    }
    return false;
  }

  // helper function:
  // determine if the input field is an array type
  // throw exceptions if the field is multi-dimention array type
  public Boolean isArray(JSONObject jsonfield) {
    JSONObject Earray = new JSONObject();
    try{
      Earray = jsonfield.getJSONObject("type");
      Earray.get("e");
    }
    catch(JSONException e){
      return false; // then it is a single element
    }
    try{
      Earray = jsonfield.getJSONObject("type");
      Earray.getString("e");
    }
    catch (JSONException e) {
      System.out.println("this program cannot handle multi-dimention array");
      e.printStackTrace();
    }
    return true;
  }

  // determine if it is array && primitive
  public Boolean isArrayPrim(JSONObject jsonfield) {
    JSONObject currType = jsonfield.getJSONObject("type");
    int k = -1;
    for (int i = 0; i < this.JsClssNms.size(); ++i) {
      if (currType.getString("e").equals(this.JsClssNms.get(i))) {
        k++;
      }
    }
    if (k == -1) {
      return true;
    }
    return false;
  }

  // determine if it needs protection texts
  public Boolean needArrayProtection(ArrayList<String> classHis) {
    if (classHis.size() > 1){
      for (int l = 0; l < classHis.size() - 1; ++l){
        if (classHis.get(l).substring(0, 1).equals("1")) {
          return true;
        }
      }
    }
    return false;
  }

  // protection code 
  public String protectionCode_first(ArrayList<String> classHis) {
    int index = 0;
    String value = "";
    String protectionCode = "";
    for (int l = 0; l < classHis.size() - 1; ++l){
      String f = "";
      if (classHis.get(l).substring(0, 1).equals("1")) {
        f = "0";
        index = l;
        break;
      }
      value += "get" + FirstCap(classHis.get(l).substring(1)) + "(" +f+").";
    }
    
    value += "num" + FirstCap(classHis.get(index).substring(1)) + "(" + ")"; 
    protectionCode += this.start + "if (" +value + " != 0){ \n";
    return protectionCode;
  }

  public String protectionCode_last(ArrayList<String> classHis, int ID, String name) {
    String protectionCode = this.start + "}\n";
    protectionCode += this.start + "else{\n";
    protectionCode += this.start + "value" + ID + ".put(\"" + name + "\"" + ", 0);\n";
    protectionCode += this.start + "}\n";
    return protectionCode;
  }
  
  
}
