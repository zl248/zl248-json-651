package edu.duke.ece651.classbuilder;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FieldParser {
  // field 1: global field array, contains all fields for a class
  public JSONArray FieldArray;
  // field 2: the number of fields in the array
  public int num;
  // field 3: the field names in the array
  private ArrayList<String> FieldNames;
  // field 4: the field types in the array
  private ArrayList<String> FieldTypes;
  // field 5: contains if the field's type is array, in order
  private ArrayList<Boolean> isArray;
  // field 6: determine if the fields has array type
  private boolean hasArray;
  // field 7: store the conversion of primitive and wrapper type
  private HashMap<String, String> Wrapper;

  // constructor
  // constructed by field array of a class
  // fill in the field name and type information
  public FieldParser(JSONArray FieldArray) {
    this.FieldArray = FieldArray;
    this.num = FieldArray.length();
    // fieldNames
    this.FieldNames = new ArrayList<String>();
    // fieldTypes
    this.hasArray = false;
    this.isArray = new ArrayList<Boolean>();
    this.FieldTypes = new ArrayList<String>();
    
    for (int i = 0; i < this.num; i++) {
      FieldNames.add(this.FieldArray.getJSONObject(i).getString("name"));
      FieldTypes.add(TypeInitializer(this.FieldArray.getJSONObject(i)));
    }
  }

  // type initializer
  public String TypeInitializer(JSONObject FieldObj) {
    this.Wrapper = new HashMap<String, String>();
    this.Wrapper.put("byte", "Byte");
    this.Wrapper.put("short", "Short");
    this.Wrapper.put("int", "Integer");
    this.Wrapper.put("long", "Long");
    this.Wrapper.put("float", "Float");
    this.Wrapper.put("double", "Double");
    this.Wrapper.put("char", "Character");
    this.Wrapper.put("boolean", "Boolean");
    String Type;
    try{
      Type = FieldObj.getString("type");
      this.isArray.add(false);
    }
    catch (JSONException e) {
      this.hasArray = true;
      Type = AdvancedTypeInitializer(FieldObj.getJSONObject("type"), 1, "");
      this.isArray.add(true);
    }
    return Type;
  }

  // a funciton to construct multi-dimentional field
  // uses a layers as recursion
  public String AdvancedTypeInitializer(JSONObject Fieldtype, int layers, String type) {
    String LayerType = "";
    type += "ArrayList<";
    try{
      LayerType = Fieldtype.getString("e");
      type += PrimToWrap(LayerType);
      for (int i = 0; i < layers; i++){
        type += ">";
      }
    }
    catch (JSONException e) {
      layers += 1;
      LayerType = AdvancedTypeInitializer(Fieldtype.getJSONObject("e"), layers, "");
      type += LayerType;
    }
    return type;
  } 
  
  public boolean hasArray() {
    return this.hasArray;
  }

  public boolean isArray(int i) {
    return isArray.get(i);
  }

  public String getFieldName(int i){
    return FieldNames.get(i);
  }

  public String getFieldType(int i){
    return FieldTypes.get(i);
  }

  // convert the type from primitive to wrapper
  public String PrimToWrap(String prim) {
    if (!this.Wrapper.containsKey(prim)) {
      return prim;
    }
    
    return this.Wrapper.get(prim);
  }
}
