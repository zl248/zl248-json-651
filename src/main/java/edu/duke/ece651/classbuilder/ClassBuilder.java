package edu.duke.ece651.classbuilder;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
//import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ClassBuilder {
  public String ClassStr; // just for test
  //private String class_str;

  //field 1: the package of the JSON
  private String JsPkg;
  //field 2: all the classes' names
  private ArrayList<String> JsClssNms;
  //field 3: JSON object as the whole file 
  private JSONObject JsObj;
  //field 4: JSON object array for the classes in the file
  private JSONArray JsClssAry;
  // object field 1: sourcecode generator 
  private SourceCodeGenerator JsCodeGtr;
  // object field 2: deserializer generator
  private DeserializerGenerator DeserGtr;
  // global identity 
  private int ID;
  
  // constructor 1
  public ClassBuilder(String str) throws JSONException {
    //this.class_str = str;
    this.ClassStr = str;
    // start parsing 
    this.JsObj = new JSONObject(str);
    // part 1: Package
    this.JsPkg = null;
    // helper function
    GenPkg();
    // part 2: the classes after the package, organized as an array
    this.JsClssAry = this.JsObj.getJSONArray("classes");
    // part 2.1: the classes' names in part 2 
    this.JsClssNms = new ArrayList<String>();
    // helper funciton
    GenClsName();
    
    // source code generator
    this.JsCodeGtr = new SourceCodeGenerator();
    // deserializer code generator
    this.DeserGtr = new DeserializerGenerator();
    // trace class ID: default 0
    this.ID = 0;
  }

  // constructor 2
  // one input argument: inputstream
  // Invoking contructor 1
  public ClassBuilder(InputStream InputStr) throws JSONException, IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(InputStr));
    String str;
    StringBuilder buffer = new StringBuilder();
    while ((str = reader.readLine()) != null) {
      buffer.append(str);
    }
    reader.close();
    String ConvrtStr = buffer.toString();
    this.JsObj = new JSONObject(ConvrtStr);
    this.JsPkg = null;
    GenPkg();
    this.JsClssAry = this.JsObj.getJSONArray("classes");
    this.JsClssNms = new ArrayList<String>();
    GenClsName();
    this.JsCodeGtr = new SourceCodeGenerator();
    this.DeserGtr = new DeserializerGenerator();
    this.ID = 0;
  }
  
  // constructors' two helper generators
  public void GenPkg(){
    if (this.JsObj.has("package")){
      this.JsPkg = this.JsObj.getString("package");
    }
  }

  public void GenClsName(){ // assist to generate things 
    // goal is to move the name into MyJsClss
    for (int i = 0; i < JsClssAry.length(); i++) {
      JSONObject TmpObj = JsClssAry.getJSONObject(i);
      JsClssNms.add(TmpObj.getString("name"));
    }
  }
 
  // method 1: return class names
  public List<String> getClassNames() {
    return JsClssNms;
  }

  // method 2: find if this class is in the collection
  // try-catch structure is a must-do way because JSON type dismatch
  // would return exception
  public String getSourceCode(String className) {
    int index = -1;
     try {
       for (int i = 0; i < JsClssAry.length(); i++) {
         JSONObject TmpObj = JsClssAry.getJSONObject(i);
         if (TmpObj.getString("name").equals(className)){
           index = i;
         }
       }
       if (index == -1){
         throw new IOException();
       } 
     } catch (IOException e) {
       System.out.println("Cannot get the source code' name:" + className);
       System.out.println(e);
     }
     
     // the specific class(object) we want to process
     JSONObject SpecJsObj = this.JsClssAry.getJSONObject(index);
     String ClssPkg = this.JsPkg;
     
     this.JsCodeGtr.setJsID(this.ID);
     this.JsCodeGtr.setJsPkg(ClssPkg);
     this.JsCodeGtr.setJsClass(className, SpecJsObj);
     this.JsCodeGtr.setJsArry(this.JsClssAry);
     this.JsCodeGtr.setJsClssNms(this.JsClssNms);
     String SrcCode = "";
     try{
       SrcCode = this.JsCodeGtr.getSrcCode();
     }
     catch (IOException e) {
       System.out.println("serialization failure");
       System.out.println(e);
     }
     return SrcCode;
  }

  // deserializer method, invokes n times if we have n differnt classes 
  public String getDeserializer(String className)throws IOException{
    int index = -1;
     try {
       for (int i = 0; i < this.JsClssAry.length(); i++) {
         JSONObject TmpObj = this.JsClssAry.getJSONObject(i);
         if (TmpObj.getString("name").equals(className)){
           index = i;
         }
       }
       if (index == -1){
         throw new IOException();
       } 
     } catch (IOException e) {
       System.out.println("Cannot get the source code' name:" + className);
       System.out.println(e);
     }
     JSONObject SpecJsObj = this.JsClssAry.getJSONObject(index);
     String ClssPkg = this.JsPkg;
     this.DeserGtr.setJsID(this.ID);
     this.DeserGtr.setJsClass(className, SpecJsObj, this.JsClssNms, this.JsClssAry);
     String DeserCode = this.DeserGtr.getDeserCode();
     return DeserCode;
  }
  
  // method 3: create one source file per class
  public void createAllClasses(String basePath) throws IOException{
    //Todo
      // save the sourcecode into different files
    if (this.JsPkg != null) {
      basePath += expandJavaDir(this.JsPkg) + "/";
    }
    for (String cls : getClassNames()){
      System.out.println(this.JsPkg);
      try (OutputStream output = new FileOutputStream(basePath + cls +".java")) {
        String TestString = getSourceCode(cls);
        output.write(TestString.getBytes("UTF-8")); // Hello
      } // compiler will invoke close()
      
    }
    // then save it to the pakage
    try (OutputStream output = new FileOutputStream(basePath+ "Deserializer.java")) {
      String deser = "package " + this.JsPkg + ";\n";
      deser +=  "import java.util.*;" + "\n";
      deser += "import org.json.*;" + "\n";
      deser += "public class Deserializer{" + "\n";
      for (String cls : getClassNames()){
        String DeserString = getDeserializer(cls);
        deser += DeserString;
      }
      
      deser += "\n" + "}";
      output.write(deser.getBytes("UTF-8")); 
    }
  }
  
  // substitue the package name as the valid directory 
  String expandJavaDir(String pak) {
    String dir = pak.replace('.', '/');
    return dir;
  }
}
